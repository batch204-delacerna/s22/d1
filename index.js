console.log("Henlo World!");

/*
	Manipulating Arrays with Array Methods
		-JavaScript has built-in functions for arrays.
		-This allows us to manipulate and access array items.
		-Arrays can either be mutated or iterated.
*/

// Mutatir Methods
/*
	-Mutator methods are functions that "mutate" or change an array after theyr're created.
	-These methods manipulate the original array by performing various tasks such as adding and removing/deleting elements.
*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Lemon'];

/*
	push()
		Adds an element in the end of an array AND returns the array's length

	Syntax:
		arrayName.push();
*/ 

console.log("Current array:");
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log("Mutated array from push method");
console.log(fruits);


// Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log(fruits);

/*
	pop()
		-Removes the last element in an array AND returns the removed element

	Syntax:
		arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method");
console.log(fruits);

/*
	unshift()
		Add one or more elements at the beginning of an array.

	Syntax:
		arrayName.unshift('elementA');
		arrayName.unshift('elementA', 'elementB');
*/

fruits.unshift('Lime', 'Banana');
console.log("Mutated array from unshift method");
console.log(fruits);

/*
	shift()
		Removes an element at the beginning of an array AND returns the removed element

	Syntax:
		arrayName.shift()
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method');
console.log(fruits);

/*
	splice()
		Simultaneously removes an element from a specified index number and adds an element

	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1,2,'Lime', 'Cherry');
// console.log(anotherFruit);
console.log('Mutated array from splice method');
console.log(fruits);

fruits.splice(1,2);
console.log(fruits);
fruits.splice(2,0);
console.log(fruits);

// fruits.splice(3);
// console.log(fruits);

/*
	sort()
		Rearranges the array element in alphanumeric order
	Syntax:
		arrayName.sor()
*/

fruits.sort();
console.log('Mutate array from sort method');
console.log(fruits);

// let arrayWithNumbers = [100, 20, "Jayson", "Carlos"];
// arrayWithNumbers.sort();
// console.log(arrayWithNumbers);


/*
	reverse()
		Reverses the order of array elements.
	Syntax:
		arrayName.reverse();
*/

// fruits.reverse();
// console.log('Mutate array from reverse method');
// console.log(fruits);

fruits.sort().reverse(); // Chain method
console.log(fruits);

// Non-Mutator Methods
/*
	-Non-Mutator methods are functions that do not modify or change an array after they're created
	-These methods do not manipulate the original array performing various tasks
*/

let countries = ['US', 'PH', 'CA', 'TH', 'HK', 'PH', 'FR', 'DE'];
/*
	indexOf()
		-Returns the index number of the first matching element found in an array.
		-If no match was found, the result will be -1.
		-The search process will be done from first element proceeding to the last element
	Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf('PH'); //1
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('HW'); // -1
console.log('Result of indexOf method: ' + invalidCountry);

/*
	lastIndexOf()
		Returns the index number of the last matching element found in an array.
		The search process will be done from last element proceeding to the first element.

	Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);
*/

let lastIndex = countries.lastIndexOf('PH'); //15
console.log('Result of indexOf method: ' + lastIndex);

console.log(countries);

/*
	slice()
		Portions/slices elements from an array AND returns a new array
	Syntax:
		arrayName.slice(startingIndex)
		arrayName.slice(startingIndex, endingIndex)
*/

// Slicing off elements from a specified index to the last element

let slicedArrayA = countries.slice(2);
console.log('Result from slice method');
console.log(slicedArrayA);
console.log(countries);

// Slicing off elements from a specified index to another index
// But the specified last index is not included in the return
// exclusive of the last element
let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice method');
console.log	(slicedArrayB);

/*
	toString()
		Returns an array as a string by commas

	Syntax:
		arrayName.toString()
*/

let stringArray = countries.toString();
console.log('Result from toString method:');
console.log(stringArray);

/*
	concat()
		Combines two array and returns the combines result

	Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhales css', 'breathe sass'];
let taskArrayC = ['get git', 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log(tasks);

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

let combinedTasks = taskArrayA.concat('smell express', 'throw react');
console.log(combinedTasks);

// Can we combine multiple arrays with elements?
let multipleTasks = taskArrayA.concat(taskArrayA, taskArrayB,'smell express', 'throw react');
console.log(multipleTasks);

/*
	join()
		-Returns an array as a string separated by specified separator string
	Syntax:
		arrayName.join('separatorString');
*/

let users = ['Jack', 'Raf', 'Daphne', 'Lexus'];

console.log(users.join()); // ,
console.log(users.join(''));
console.log(users.join(' - '));

// Iteration Methods
/*
	Iteration methods are loops designed to perform repetitive task on array
	Iteration methods loops over all items in an array.
*/
/*
	forEach()
		Similar to a for loop that iterates on each array element.
		For each item in the array, the anonymous function passed in the forEach() method will be run.
	Syntax:
		arrayName.forEach(function(indivElement)
			{
				statement;
			});
			
*/
console.log(multipleTasks)
multipleTasks.forEach
	(
		function(task)
			{
				console.log(task);
			}
	);

// for (let i = 0; i < multipleTasks.length; i++) 
// 	{
// 		console.log(multipleTasks[i]);
// 	}